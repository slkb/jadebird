package accp.myinterface;

public class Handset {

	private String brand;
	private String type;

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void sendInfo() {
		System.out.println("发短信");
	}

	public void call() {
		System.out.println("打电话");

	}

	public void info() {
		System.out.println(String.format("您现在使用的手机是%s,型号为：%s", this.brand,this.type));

	}

}
