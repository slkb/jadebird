package accp.myinterface.ink;

public interface Ink {

	String getColor();
}
