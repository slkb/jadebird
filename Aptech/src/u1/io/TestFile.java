package u1.io;

import java.io.File;
import java.io.IOException;

public class TestFile {

	public static void main(String[] args) {
//		File file = new File("F:");
//		if (file.isDirectory()) {
//			for (String path : file.list()) {
//				System.out.println(path);
//			}
//
//		}
		
		File f=new File("F:\\txt.txt");
		TestFile t=new TestFile();
		t.fileInfo(f);
	}

	/**
	 * 
	 * @param f
	 * @return
	 * @throws IOException
	 */
	public boolean createFile(File f) throws IOException {
		if (f.exists()) {
			System.out.println("文件已存在");
			return false;
		} else {
			return f.createNewFile();
		}
	}
	
	public void fileInfo(File f){
		if (f.exists()) {
			if (f.isFile()) {
				
				System.out.println(String.format("文件名：%s", f.getName()));
				System.out.println(String.format("文件相对路径：%s", f.getPath()));
				System.out.println(String.format("文件绝对路径：%s", f.getAbsolutePath()));
				System.out.println(String.format("文件长度：%s", f.length()));
			}else{
				
				System.out.println("这不是个文件");
			}
		}else{
			System.out.println("文件不存在");
		}
	}
}
