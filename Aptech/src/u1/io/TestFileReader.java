package u1.io;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class TestFileReader {

	public static void main(String[] args) {
		FileReader r = null;
		try {
			r = new FileReader("F:\\filewriter.txt");
			char[] ch = new char[1024];
			StringBuffer sb = new StringBuffer();
			// int data = -1;
			while (r.read(ch) != -1) {
				// System.out.print(data);
				// Thread.sleep(1000);
				sb.append(ch);
				// System.out.println(ch);
			}
			System.out.println(sb.toString());

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
