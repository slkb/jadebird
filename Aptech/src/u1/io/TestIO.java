package u1.io;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * FileInputStream读文件处理中文乱码
 * 
 * @author 孟凡鼎
 *
 */
public class TestIO {

	public static void main(String[] args) {
		FileInputStream in = null;
		InputStreamReader isr = null;
		// BufferedReader br = null;
		try {
			in = new FileInputStream("F:\\txt.txt");
			isr = new InputStreamReader(in, "utf-8");
			// br = new BufferedReader(isr);

			// System.out.println(in.available());
			int data;
			while ((data = isr.read()) != -1) {
				Thread.sleep((long) (Math.random()*80));
				System.out.print((char) data);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

}
