package u1.io;

import java.io.Serializable;

public class Dog implements Serializable {

	private static final long serialVersionUID = 7096475389337729361L;

	private String name;

	private String sex;

	public Dog(String name, String sex) {
		super();
		this.name = name;
		this.sex = sex;
	}

	public String getName() {
		return name;
	}

	public String getSex() {
		return sex;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	@Override
	public String toString() {
		return String.format("狗的名字%s,狗的性别%s", this.name, this.sex);
	}

}
