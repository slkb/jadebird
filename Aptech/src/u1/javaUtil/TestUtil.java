package u1.javaUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

public class TestUtil {

	public static void main(String[] args) {
		// byte b;
		// boolean bool;
		// char ch;
		// short sh;
		// int i;
		// long l;
		// float f;
		// double db;

		// byte _byte=110;
		// Byte by = new Byte("-128");
		// System.out.println(by.byteValue());
		// System.out.println(by.MAX_VALUE);
		// System.out.println(by.MIN_VALUE);
		// System.out.println(by.SIZE);

		// boolean _b=false;
		// Boolean _bool=new Boolean("yes");
		// System.out.println(_bool);

		// char _char='B';
		// System.out.println(_char);
		// Character _character=new Character(_char);
		// System.out.println(_character.charValue());
		// System.out.println(_character.BYTES);
		// System.out.println(_character.MAX_VALUE);
		// System.out.println(_character.MIN_VALUE);

		// short _s = 32767;
		// Short s=new Short(_s);
		// System.out.println(s.shortValue());
		// System.out.println(Short.MAX_VALUE);
		// System.out.println(Short.MIN_VALUE);

		// int i =20315454;
		// Integer in=new Integer(i);
		// System.out.println(i);
		// System.out.println(Integer.toHexString(i));
		// System.out.println(Integer.MAX_VALUE);
		// System.out.println(Integer.MIN_VALUE);
		// System.out.println(Integer.compare(5, 5));
		// System.out.println(Integer.min(1212, 22));
		// System.out.println(Integer.parseInt("111"));

		// Random r=new Random(0);
		// System.out.println(r.nextInt(105));
		// Random r1=new Random(0);
		// System.out.println(r1.nextInt(105));

		// String str=String.format("现在的时间是：%tr , 注意", new Date());
		// String str = String.format("你好啊", new Date());
		// System.out.println(str.length());
		//
		// String temp="";
		// temp.equals(null);
		// Object o=new Object();
		// o.equals(o);
		//
		// temp="asdfsafasdf";
		// System.out.println(temp.equalsIgnoreCase("asDF"));
		// System.out.println(temp.toLowerCase());
		// System.out.println(temp.toUpperCase());

		// String str=" 123456789 ";
		// System.out.println(str);
		// System.out.println(str.trim());
		// System.out.println(str.substring(1));

		// String email="mengfanding@zbitedu.cn\nasdf\nasdf";
		// System.out.println(email.indexOf("@"));
		// System.out.println(email.split("@"));
		// System.out.println(email.split("@")[0]);
		// System.out.println(email.split("@")[1]);

		// String email="mengfanding@zbitedu.cn\nasdf\nasdf";
		// String[] st=email.split("\n");
		// System.out.println(st[0]);
		// System.out.println(st[1]);

		// StringBuffer sb=new StringBuffer();
		// for (int i = 0; i < 1000; i++) {
		// sb.append(i);
		// }
		// System.out.println(sb.toString());

		// StringBuilder sb = new StringBuilder();
		// for (int i = 0; i < 1000; i++) {
		// sb.append(i);
		// }
		// System.out.println(sb.toString());

		// Date date = new Date();
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// System.out.println(sdf.format(date));
		
		Calendar c=Calendar.getInstance();
		c.setTime(new Date());
		System.out.println(c.getTime());
		
		
		

	}

}
