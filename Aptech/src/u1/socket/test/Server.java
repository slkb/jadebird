package u1.socket.test;

import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	public static void main(String[] args) {
		try {
			ServerSocket server = new ServerSocket(7800);
			Socket socket = server.accept();

			InputStream in = socket.getInputStream();
			// int data;
			// while ((data=in.read())!=-1) {
			// System.out.println((char)data);
			// }
			char[] data = new char[1024];
			StringBuffer sb = new StringBuffer();
			InputStreamReader isr = new InputStreamReader(in);
			
			while (isr.read(data) != -1) {
				sb.append(data);
			}
			in.close();
			socket.close();
			server.close();

			System.out.println(sb.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
