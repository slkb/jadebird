package u1.socket;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {

	public static void main(String[] args) {
		try {
			Socket s=new Socket("localhost", 7800);
			OutputStream os=s.getOutputStream();
			Scanner input=new Scanner(System.in);
			String info=input.next();
			os.write(info.getBytes());
			os.close();
			s.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
