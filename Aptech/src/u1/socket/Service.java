package u1.socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class Service {

	public static void main(String[] args) {
		try {
			ServerSocket ss = new ServerSocket(8800);
			
			Socket s;
			InputStream is;
			BufferedReader br = null;
			while (true) {
				s = ss.accept();
				is = s.getInputStream();
				br = new BufferedReader(new InputStreamReader(is));
				String info = null;
				
				while ((info = br.readLine()) != null) {
					System.out.println(s.getInetAddress()+":" + info);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
