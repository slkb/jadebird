package u1.xml;

import java.io.FileOutputStream;
import java.io.IOException;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

public class DemoXML {

	public static void main(String[] args) throws DocumentException, IOException {
		// File f=new File("/test.xml");
		// if (!f.exists()) {
		// f.createNewFile();
		// }
		// SAXReader reader=new SAXReader();
		// Document doc=reader.read(f);
		// Document root=DocumentHelper.createDocument();

		Document doc = DocumentHelper.createDocument();

		Element root = DocumentHelper.createElement("project");

		doc.add(root);

		root.addAttribute("xmlns", "454545");
		Element dependencies = DocumentHelper.createElement("dependencies");
		root.add(dependencies);
		Element dependency=DocumentHelper.createElement("dependency");
		dependencies.add(dependency);
		

		OutputFormat of = new OutputFormat(" ", true);
		of.setEncoding("utf-8");
		XMLWriter xw = new XMLWriter(new FileOutputStream("test.xml"), of);
		xw.write(doc);
		xw.close();

	}
}
