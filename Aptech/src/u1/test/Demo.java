package u1.test;

import java.util.Scanner;

public class Demo {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.print("请输入班级总人数：");
		int number = input.nextInt();
		int[] scores = new int[number];
		int sum = 0;
		for (int i = 0; i <= scores.length - 1; i++) {
			System.out.print("请输入第" + (i + 1) + "位学生的成绩：");
			scores[i] = input.nextInt();
			if (scores[i] >= 80) {
				sum++;

			}

		}
		double avg = sum / (double)scores.length * 100;
		System.out.println("80分以上同学有： " + sum);
		
		System.out.println("80分以上同学所占比例是： " + avg + "%");
	}
}