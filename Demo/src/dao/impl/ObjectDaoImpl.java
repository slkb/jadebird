package dao.impl;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import dao.ObjectDao;

public class ObjectDaoImpl extends HibernateDaoSupport implements ObjectDao {

	@Override
	public List<Object> selectAll(Class clazz) {
		return super.getHibernateTemplate().find("from "+clazz.getSimpleName());
	}

	@Override
	public Object selectOne(Object obj) {
		return null;
	}

	@Override
	public boolean delete(Object obj) {
		return false;
	}

	@Override
	public boolean updata(Object obj) {
		return false;
	}

	@Override
	public boolean insert(Object obj) {
		super.getHibernateTemplate().save(obj);
		return true;
	}

}
