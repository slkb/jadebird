package dao;

import java.util.List;

public interface ObjectDao {

	// 增删改查

	/**
	 * 查询所有
	 * 
	 * @return
	 */
	List<Object> selectAll(Class clazz);

	/**
	 * 查询单个
	 * 
	 * @param obj
	 * @return
	 */
	Object selectOne(Object obj);

	/**
	 * 删除一个对象
	 * 
	 * @param obj
	 * @return
	 */
	boolean delete(Object obj);

	/**
	 * 修改对象
	 * 
	 * @param obj
	 * @return
	 */
	boolean updata(Object obj);

	/**
	 * 新增一个对象
	 * 
	 * @param obj
	 * @return
	 */
	boolean insert(Object obj);

}
