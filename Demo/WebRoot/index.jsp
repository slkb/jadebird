<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>首页</title>
</head>
<body>
	<center>
		<h1>查询所有</h1>
		<hr>
		<s:iterator value="list" id="i">
			<s:property value="i" />
		</s:iterator>

		<table border="1px" width="800px">
			<thead style="text-align: center;background-color: gray;">
				<tr>
					<td>编号</td>
					<td>姓名</td>
					<td>操作</td>
				</tr>
			</thead>
			<tr>
				<td>1</td>
				<td>孟凡鼎</td>
				<td><a href="#">删除</a></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td></td>
			</tr>
		</table>
		<s:debug></s:debug>
	</center>
</body>
</html>