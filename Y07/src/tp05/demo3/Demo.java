package tp05.demo3;

/**
 * 使用do-while实现：输出摄氏温度与华氏温度的对照表，要求它从摄氏温度0度到250度，每隔20度为一项，对照表中的条目不超过10条。
 * 转换关系：华氏温度 = 摄氏温度 * 9 / 5.0 + 32
 * 
 * @author 孟凡鼎
 *
 */
public class Demo {

	public static void main(String[] args) {

		int she = 0;
		int sum = 0;
		do {
			System.out.println("摄氏度：" + she);
			System.out.println("华氏度：" + (she * 9 / 5.0 + 32));
			she += 20;
			sum++;
		} while (sum <= 10 && she <= 250);
	}

}
