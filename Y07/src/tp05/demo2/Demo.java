package tp05.demo2;

/**
 * 编程实现：计算100以内（包括100）的偶数之和 设置断点并调试程序，观察每一次循环中变量值的变化
 * 
 * @author 孟凡鼎
 *
 */
public class Demo {

	public static void main(String[] args) {
		int sum =0;
		for (int i = 0; i <= 100; i+=2) {
			sum+=i;
		}
		System.out.println(sum);
		
//		int sum1=0;
//		for (int i = 0; i <= 100; i++) {
//			if (i%2==0) {
//				sum1+=i;
//			}
//		}
//		System.out.println(sum1);
	}
}
