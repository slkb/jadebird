/**
 * Y07循环
 */
package tp05.demo1;

/**
 * 2012年培养学员25万人，每年增长25%，请问按此增长速度，到哪一年培训学员人数将达到100万人？
 * 
 * @author 孟凡鼎
 *
 */
public class Demo {

	public static void main(String[] args) {
		double sum = 25;
		int year = 2012;
		boolean isok = true;
		while (isok) {
			sum *= 1.25;
			year++;
			if (sum >= 100) {
				System.out.println("今年是：" + year);
				isok = false;
			}
		}
	}
}
