package service.impl;

import java.util.List;

import dao.StDao;
import entity.St;
import service.StService;

public class StServiceImpl implements StService {

	private StDao stDao;

	public StDao getStDao() {
		return stDao;
	}

	public void setStDao(StDao stDao) {
		this.stDao = stDao;
	}

	@Override
	public void save(St s) {
		stDao.save(s);
	}

	@Override
	public List<St> findList(String keys, int pageIndex, int pageSize) {

		return stDao.findByPage(keys, pageIndex, pageSize);
	}

	@Override
	public St findOne(St s) {
		return stDao.findOne(s.getId());
	}

	@Override
	public void updata(St s) {
		stDao.updata(s);
	}

	@Override
	public void delete(Integer id) {
		St s = stDao.findOne(id);
		stDao.deleteOne(s);
	}

	@Override
	public int getPageCount(String keys, int pageSize) {
		int listCount = stDao.getListCount(keys);

		if (pageSize == 0) {
			return 1;
		} else {
			return listCount % pageSize == 0 ? listCount / pageSize : listCount
					/ pageSize + 1;
		}

	}

	@Override
	public void delete(String ids) {
		stDao.deleteList(ids);
	}

}
