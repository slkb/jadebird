package service.impl;

import java.util.List;

import dao.PersonDao;
import entity.Person;
import service.PersonService;

public class PersonServiceImpl implements PersonService {

	private PersonDao dao;

	public PersonDao getDao() {
		return dao;
	}

	public void setDao(PersonDao dao) {
		this.dao = dao;
	}

	@Override
	public void save(Person obj) {
		dao.save(obj);
	}

	@Override
	public List<Person> findList(String keys, int pageIndex, int pageSize) {
		int firstIndex = 0;
		if (pageIndex > 1) {
			firstIndex = (pageIndex - 1) * pageSize;
		}
		return dao.findByPage(keys, firstIndex, pageSize);
	}

	@Override
	public int getPageCount(String keys, int pageSize) {
		int listCount = dao.getListCount(keys);
		if (pageSize == 0) {
			pageSize = 5;
		}
		if (listCount <= pageSize) {
			return 1;
		} else {
			return listCount % pageSize == 0 ? (listCount / pageSize)
					: (listCount / pageSize + 1);
		}
	}

	@Override
	public Person findOne(Person obj) {
		return dao.findOne(obj);
	}

	@Override
	public void updata(Person obj) {
		dao.updata(obj);
	}

	@Override
	public void delete(Person obj) {
		dao.deleteOne(obj);
	}

}
