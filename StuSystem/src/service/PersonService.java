package service;

import java.util.List;

import entity.Person;

public interface PersonService {


	void save(Person obj);

	List<Person> findList(String keys, int pageIndex, int pageSize);
	
	int getPageCount(String keys,int pageSize);
	
	Person findOne(Person obj);
	
	void updata(Person obj);
	
	void delete(Person obj);
	
}
