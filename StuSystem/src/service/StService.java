package service;

import java.util.List;

import entity.St;

public interface StService {

	void save(St s);

	List<St> findList(String keys, int pageIndex, int pageSize);
	
	int getPageCount(String keys,int pageSize);
	
	St findOne(St s);
	
	void updata(St s);
	
	void delete(Integer id);
	
	void delete(String ids);

}
