package dao;

import java.util.List;

import entity.St;

public interface StDao {

	void save(St s);

	List<St> findByPage(String keys, int pageIndex, int pageSize);

	int getListCount(String keys);

	St findOne(Integer id);

	void updata(St s);

	void deleteOne(St s);
	
	void deleteList(String ids);

}
