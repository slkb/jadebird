package dao;

import java.util.List;

import entity.Person;
import entity.St;

public interface PersonDao {

	void save(Person obj);

	List<Person> findByPage(String keys, int firstIndex, int pageSize);

	int getListCount(String keys);

	Person findOne(Person obj);

	void updata(Person obj);

	void deleteOne(Person obj);
}
