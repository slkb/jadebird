package dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import dao.StDao;
import entity.St;

public class StDaoImpl implements StDao {

	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	@Override
	public void save(St s) {

		sessionFactory.getCurrentSession().save(s);

	}

	@Override
	public List<St> findByPage(String keys, int pageIndex, int pageSize) {
		String hql = "from St ";
		if (keys != null && !keys.isEmpty()) {
			hql += " where keys like '%" + keys + "%' or zhname like '%" + keys
					+ "%' ";
		}
		hql+=" order by id desc";
		return sessionFactory.getCurrentSession().createQuery(hql)
				.setFirstResult(pageIndex).setMaxResults(pageSize).list();
	}

	@Override
	public St findOne(Integer id) {
		return (St) sessionFactory.getCurrentSession()
				.createQuery("from St where id = :id ").setInteger("id", id)
				.uniqueResult();
	}

	@Override
	public void updata(St s) {
		sessionFactory.getCurrentSession().update(s);
	}

	@Override
	public void deleteOne(St s) {
		sessionFactory.getCurrentSession().delete(s);
	}

	@Override
	public int getListCount(String keys) {

		String hql = "from St";
		if (keys != null && !keys.isEmpty()) {
			hql += " where keys like '%" + keys + "%' or zhname like '%" + keys
					+ "%'";
		}

		int count = sessionFactory.getCurrentSession().createQuery(hql).list()
				.size();

		return count;
	}

	@Override
	public void deleteList(String ids) {
		String sql="delete from standard where id in("+ids+")";
		Session session=sessionFactory.openSession();
		session.createSQLQuery(sql).executeUpdate();
		session.close();
		//sessionFactory.getCurrentSession().createSQLQuery(sql);
	}

}
