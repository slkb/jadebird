package dao.impl;

import java.util.List;

import org.hibernate.SessionFactory;

import dao.PersonDao;
import entity.Person;
import entity.St;

public class PersonDaoImpl implements PersonDao {

	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@Override
	public void save(Person obj) {
		sessionFactory.getCurrentSession().save(obj);
	}

	@Override
	public List<Person> findByPage(String keys, int firstIndex, int pageSize) {
		String hql = "from Person ";
		if (keys != null && !keys.isEmpty()) {
			hql += " where name like '%" + keys + "%'";
		}
		return sessionFactory.getCurrentSession().createQuery(hql).setFirstResult(firstIndex).setMaxResults(pageSize).list();
	}

	@Override
	public int getListCount(String keys) {
		String hql = "from Person ";
		if (keys != null && !keys.isEmpty()) {
			hql += " where name like '%" + keys + "%'";
		}
		return sessionFactory.getCurrentSession().createQuery(hql).list().size();
	}

	@Override
	public Person findOne(Person obj) {
		return (Person) sessionFactory.getCurrentSession().get(obj.getClass(), obj.getId());
	}

	@Override
	public void updata(Person obj) {
		sessionFactory.getCurrentSession().update(obj);
	}

	@Override
	public void deleteOne(Person obj) {
		sessionFactory.getCurrentSession().delete(obj);
	}

}
