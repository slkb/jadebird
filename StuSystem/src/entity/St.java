package entity;

import java.util.Date;

/**
 * St entity. @author MyEclipse Persistence Tools
 */

public class St implements java.io.Serializable {

	// Fields

	private Integer id;
	private String stdNum;
	private String zhname;
	private String version;
	private String keys;
	private Date releaseDate;
	private Date implDate;
	private String packagePath;

	// Constructors

	/** default constructor */
	public St() {
	}

	/** minimal constructor */
	public St(String zhname, String version, String keys, String packagePath) {
		this.zhname = zhname;
		this.version = version;
		this.keys = keys;
		this.packagePath = packagePath;
	}

	/** full constructor */
	public St(String stdNum, String zhname, String version, String keys,
			Date releaseDate, Date implDate, String packagePath) {
		this.stdNum = stdNum;
		this.zhname = zhname;
		this.version = version;
		this.keys = keys;
		this.releaseDate = releaseDate;
		this.implDate = implDate;
		this.packagePath = packagePath;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getStdNum() {
		return this.stdNum;
	}

	public void setStdNum(String stdNum) {
		this.stdNum = stdNum;
	}

	public String getZhname() {
		return this.zhname;
	}

	public void setZhname(String zhname) {
		this.zhname = zhname;
	}

	public String getVersion() {
		return this.version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getKeys() {
		return this.keys;
	}

	public void setKeys(String keys) {
		this.keys = keys;
	}

	public Date getReleaseDate() {
		return this.releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Date getImplDate() {
		return this.implDate;
	}

	public void setImplDate(Date implDate) {
		this.implDate = implDate;
	}

	public String getPackagePath() {
		return this.packagePath;
	}

	public void setPackagePath(String packagePath) {
		this.packagePath = packagePath;
	}

}