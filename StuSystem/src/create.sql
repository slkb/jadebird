-- Create table
create table STANDARD
(
  id           NUMBER not null,
  std_num      VARCHAR2(50),
  zhname       VARCHAR2(40) not null,
  version      VARCHAR2(10) not null,
  keys         VARCHAR2(50) not null,
  release_date DATE,
  impl_date    DATE,
  package_path VARCHAR2(100)
)
tablespace SYSTEM
  pctfree 10
  pctused 40
  initrans 1
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
-- Create/Recreate primary, unique and foreign key constraints 
alter table STANDARD
  add constraint P_ID primary key (ID)
  using index 
  tablespace SYSTEM
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
alter table STANDARD
  add constraint U_ST unique (STD_NUM)
  using index 
  tablespace SYSTEM
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64K
    minextents 1
    maxextents unlimited
  );
