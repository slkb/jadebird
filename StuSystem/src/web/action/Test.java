package web.action;

import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import entity.Person;
import entity.St;
import service.PersonService;
import service.StService;

public class Test {

	public static void main(String[] args) {
		ApplicationContext ac=new ClassPathXmlApplicationContext("applicationContext.xml");
		PersonService ps=(PersonService) ac.getBean("pService");
		Person p=new Person();
		p.setAge(12);
		p.setBirthday(new Date());
		p.setImgPath("asdf");
		p.setName("����");
		ps.save(p);
		
		StService ss=(StService) ac.getBean("stService");
		St st=new St();
		st.setImplDate(new Date());
		st.setKeys("asdf");
		st.setPackagePath("asdfasdf");
		st.setReleaseDate(new Date());
		st.setStdNum("asdfSADFasdf");
		st.setVersion("asdf");
		st.setZhname("asdfasdfsadf");
		ss.save(st);
	}
}
