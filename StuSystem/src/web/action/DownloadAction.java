package web.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.struts2.ServletActionContext;

import service.StService;

import com.opensymphony.xwork2.ActionSupport;

import entity.St;

public class DownloadAction extends ActionSupport {

	private StService stService;
	
	public StService getStService() {
		return stService;
	}

	public void setStService(StService stService) {
		this.stService = stService;
	}

	private St st;
	

	public St getSt() {
		return st;
	}

	public void setSt(St st) {
		this.st = st;
	}

	private String fileName;
	private InputStream inputStream;
	
	public String getFileName() throws UnsupportedEncodingException {
		return new String(fileName.getBytes(), "iso-8859-1");
	}

	public void setFileName(String fileName) throws UnsupportedEncodingException {
		this.fileName = fileName;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String download() throws FileNotFoundException{
		St s=stService.findOne(st);
		
		String path=ServletActionContext.getServletContext().getRealPath("/upload");
		
		File f=new File(path+"/"+s.getPackagePath());
		
		this.inputStream=new FileInputStream(f);
		
		return SUCCESS;
	}
}
