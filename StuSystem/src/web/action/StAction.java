package web.action;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.ServletActionContext;

import service.StService;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import entity.St;

public class StAction extends ActionSupport {

	private StService stService;
	private St st;
	private String keys;
	private Integer pageIndex;
	private Integer pageSize;
	private File file;
	private String fileFileName;
	private String ids;
	

	

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}
	private St upSt;
	private Integer delId;

	public Integer getDelId() {
		return delId;
	}

	public void setDelId(Integer delId) {
		this.delId = delId;
	}

	public St getUpSt() {
		return upSt;
	}

	public void setUpSt(St upSt) {
		this.upSt = upSt;
	}

	public String getKeys() {
		return keys;
	}

	public void setKeys(String keys) {
		this.keys = keys;
	}

	public Integer getPageIndex() {
		return pageIndex;
	}

	public void setPageIndex(Integer pageIndex) {
		this.pageIndex = pageIndex;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public St getSt() {
		return st;
	}

	public void setSt(St st) {
		this.st = st;
	}

	public StService getStService() {
		return stService;
	}

	public void setStService(StService stService) {
		this.stService = stService;
	}

	public String save() throws IOException {

		String filePath = ServletActionContext.getServletContext().getRealPath(
				"/upload");
		File newFile = new File(filePath, fileFileName);
		FileUtils.copyFile(file, newFile);

		st.setPackagePath(newFile.getName());

		stService.save(st);

		return SUCCESS;
	}

	public String findList() {

		if (pageIndex == null) {
			pageIndex = 1;

		}
		if (pageSize == null) {
			pageSize = 5;
		}
		ActionContext.getContext().put("list",
				stService.findList(keys, (pageIndex - 1) * pageSize, pageSize));
		ActionContext.getContext().put("pageCount", stService.getPageCount(keys, pageSize));
		//ServletActionContext.getRequest().setAttribute(name, o);

		return SUCCESS;

	}

	public String findOne() {
		ActionContext.getContext().put("upSt", stService.findOne(upSt));
		return SUCCESS;
	}

	public String updata() {
		stService.updata(st);
		return SUCCESS;
	}

	public String delete() {
		stService.delete(delId);
		return SUCCESS;
	}
	public String deleteList(){
		stService.delete(ids);
		return SUCCESS;
	}
}
