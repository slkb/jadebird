<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
	
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>添加</title>
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript">

$(function() {

});

function check(form){
	var stdNum=$("input[name='st.stdNum']").val();
			var keys=$("input[name='st.keys']").val();
			var packagePath=$("input[name='st.packagePath']").val();
			var releaseDate=$("input[name='st.releaseDate']").val();
			var version=$("input[name='st.version']").val();
			var zhname=$("input[name='st.zhname']").val();
			var file=$("input[name='file']").val();
			if (stdNum=="") {
				alert("标准号不能为空");
				return false;
			}else if(zhname=="") {
				alert("中文名不能为空");
				return false;
			}else if(version=="") {
				alert("版本号不能为空");
				return false;
			}else if(keys=="") {
				alert("关键字不能为空");
				return false;
			}else if(file=="") {
				alert("必须上传文档");
				return false;
			}
}
</script>
</head>
<body>
	<center>
	
	<h1>标准信息</h1>
	<hr>
		<form action="add.action" method="post" enctype="multipart/form-data">

			<table style="" border="1px">
				<tr>
					<td colspan="2"> &nbsp; </td>
				</tr>
				<tr>
					<td style="width: 180px; text-align: right; background-color: #D7D7D7">* 标准号：</td>
					<td><input name="st.stdNum"></td>
				</tr>
				<tr>
					<td style="width: 180px; text-align: right; background-color: #D7D7D7">* 中文名称：</td>
					<td><input name="st.zhname"></td>
				</tr>
				<tr>
					<td style="width: 180px; text-align: right; background-color: #D7D7D7">* 版本：</td>
					<td><input name="st.version"></td>
				</tr>
				<tr>
					<td style="width: 180px; text-align: right; background-color: #D7D7D7">* 关键字/词：</td>
					<td><input name="st.keys"></td>
				</tr>
				<tr>
					<td style="width: 200px; text-align: right; background-color: #D7D7D7">发布日期（yyyy-MM-dd）：</td>
					<td><input name="st.releaseDate"></td>
				</tr>
				<tr>
					<td style="width: 180px; text-align: right; background-color: #D7D7D7">实施日期（yyyy-MM-dd）：</td>
					<td><input name="st.implDate"></td>
				</tr>
				<tr>
					<td style="width: 180px; text-align: right; background-color: #D7D7D7">* 附件</td>
					<td><input type="file" name="file"></td>
				</tr>
				<tr>
					<td colspan="2" style="text-align: center;"><input
						type="submit" onclick="return check(this.form)" value="提交"><input type="button" value="取消" onclick="script:window.location.href='/demo'"></td>
				</tr>
			</table>
		</form>
	</center>
</body>
</html>