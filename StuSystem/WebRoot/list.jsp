<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>

<title>index</title>
<meta content="text/html;charset=utf-8">
<script type="text/javascript" src="js/jquery-1.8.3.js"></script>
<script type="text/javascript">

	$(function() {
		$("#all").click(function() {

			if (this.checked) {
				$("input[name='standardId']").attr("checked", true);
			} else {
				$("input[name='standardId']").attr("checked", false);
			}

		});

	});

	function search() {
		var keys = $("#search").val();
		window.location.href = "/demo/findList.action?keys=" + keys;
	}

	function deleteList() {
		if($("input[name='standardId']:checked").size()==0){
			alert("至少要选择一个删除");
			return;
		}

		if (confirm("确定要删除？")) {
			var checkedList = new Array();
			$("input[name='standardId']:checked").each(function() {
				checkedList.push($(this).val());
			});
			window.location.href = "deleteList.action?ids=" + checkedList;
		}

	}
</script>
</head>

<body>
	<center>
		<h1>标准信息列表</h1>
		<hr>
		<table border="1px" style="width: 1000px; text-align: center; ">
			<thead>
				<tr>
					<td colspan="7" style="text-align: right;">
						<input id="search"> 
						<input type="button" value="提交检索" onclick="search()">
						<input type="button" value="新增标准" onclick="script:window.location.href='/demo/add.jsp'">
						<input type="button" value="删除标准" onclick="deleteList()">
					</td>
				</tr>
			</thead>
			<tr style="background-color: #D7D7D7; text-align: center;">
				<td><input type="checkbox" id="all"></td>
				<td>标准号</td>
				<td>中文名称</td>
				<td>版本</td>
				<td>发布日期</td>
				<td>实施日期</td>
				<td>操作</td>
			</tr>
			
	
			
			<s:iterator value="list" id="i" status="ii">
				<tr <s:if test="#ii.index%2==1">style="background-color: #D7D7D7"</s:if>>

					<td>
					  <input type="checkbox" name="standardId" value="<s:property value="#i.id" />">
					</td>
					
					<td><a href="findOne.action?upSt.id=<s:property value="#i.id" />"><s:property value="#i.stdNum" /></a></td>
					<td><s:property value="#i.zhname" /></td>
					<td><s:property value="#i.version" /></td>
					<td><s:date name="#i.releaseDate" format="yyyy-MM-dd"/></td>
					<td><s:date name="#i.implDate" format="yyyy-MM-dd"/></td>
					<td><a href="download.action?st.id=<s:property value="#i.id" />">下载</a> <a href="findOne.action?upSt.id=<s:property value="#i.id" />">修改</a> <a href="delete.action?delId=<s:property value="#i.id" />">删除</a></td>
				</tr>
			</s:iterator>

			<tfoot>
				<tr>
					<td colspan="7">
					<s:if test="pageIndex==1">
						首页 | 
						上一页 |
					</s:if>
					<s:else>
					<a href="findList.action?keys=<s:property value="keys" />">首页 </a> | 
					<a href="findList.action?keys=<s:property value="keys" />&pageSize=<s:property value="pageSize" />&pageIndex=<s:property value="pageIndex-1" />">上一页</a>
					</s:else>
					<s:if test="pageIndex==#pageCount">
						下一页 |
						末页
					</s:if>
					<s:else>
					<a href="findList.action?keys=<s:property value="keys" />&pageSize=<s:property value="pageSize" />&pageIndex=<s:property value="pageIndex+1" />">下一页</a> |
					<a href="findList.action?keys=<s:property value="keys" />&pageSize=<s:property value="pageSize" />&pageIndex=<s:property value="#pageCount" />">末页</a> 
					</s:else>
					 第<s:property value="pageIndex" />页/共<s:property value="#pageCount" />页
					 
					 </td>
				</tr>
			</tfoot>
		</table>
		<s:debug></s:debug>
	</center>
</body>
</html>
