package u1_3.c3;

public class Demo {

	public static void main(String[] args) {
		String email = "love@mengfanding.cn";
		System.out.println(email.substring(1,3));
		System.out.println(email.indexOf('@'));
		System.out.println(email.indexOf('.'));
		
		String poem = "床前明月光, 疑是地上霜, 举头望明月, 低头思故乡";
		for (String string : poem.split(",")) {
			System.out.println(string);
		}
		
		String[] temp = poem.split("明");
		for (String str : temp) {
			System.out.println(str.length());
		}
		
	}
}
