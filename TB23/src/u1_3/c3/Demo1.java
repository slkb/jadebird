package u1_3.c3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Demo1 {

	public static void main(String[] args) {
		
		System.out.println(getDate(new Date()));
		
	}
	
	public static String getDate(Date date){
		SimpleDateFormat sdf =new SimpleDateFormat("yy-M-d ah:m:s  今年第w周 / 本月第W周  D  F");
		SimpleDateFormat s =new SimpleDateFormat("y-M-d h:m:s");
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(s.parse("2016-2-11 13:0:0"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
//		c.set(Calendar.HOUR_OF_DAY, 0);
//		c.set(Calendar.MINUTE, 0);
//		c.set(Calendar.SECOND, 0);
//		c.set(Calendar.MILLISECOND, 0);
//		c.add(Calendar.DAY_OF_MONTH, -1);
		return sdf.format(c.getTime());
//		return c.getTime();
		
	}
	
}
