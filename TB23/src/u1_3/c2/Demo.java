package u1_3.c2;

enum Light {
	RED, YELLOW, GREEN
}

public class Demo {

	public static void main(String[] args) throws InterruptedException {
		Light light = Light.RED;
		while (true) {

			switch (light) {
			case RED:
				System.out.println("现在是红灯");
				for (int i = 10; i > 0; i--) {
					System.out.println("还有"+i+"秒");
					Thread.sleep(1000);
				}
				light=Light.YELLOW;
				break;
			case YELLOW:
				System.out.println("现在是黄灯");
				for (int i = 5; i > 0; i--) {
					System.out.println("还有"+i+"秒");
					Thread.sleep(1000);
				}
				light=Light.GREEN;
				break;
			case GREEN:
				System.out.println("现在是绿灯");
				for (int i = 10; i > 0; i--) {
					System.out.println("还有"+i+"秒");
					Thread.sleep(1000);
				}
				light=Light.RED;
				break;
			}
		}
	}
}
