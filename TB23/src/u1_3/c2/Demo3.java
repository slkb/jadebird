package u1_3.c2;

import java.util.ArrayList;

public class Demo3 {
	public static void main(String[] args) {
		ArrayList list = new ArrayList();
		list.add(1);
		list.add(2f);
		
		for (Object object : list) {
			System.out.println(object);
		}
	}

}
