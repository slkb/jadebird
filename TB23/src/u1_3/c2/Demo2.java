package u1_3.c2;

import static org.fusesource.jansi.Ansi.ansi;
import static org.fusesource.jansi.Ansi.Color.GREEN;
import static org.fusesource.jansi.Ansi.Color.RED;

import org.fusesource.jansi.AnsiConsole;
public class Demo2 {

	public static void main(String[] args) {
		AnsiConsole.systemInstall();
		System.out.println( ansi().eraseScreen().fg(RED).a("Hello").fg(GREEN).a(" World").reset() );
		System.out.println( ansi().eraseScreen().render("@|red Hello|@ @|green World|@") );
	}
}
