package u1_3.c4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Date;

public class Demo {

	public static void main(String[] args) {
		Demo d =new Demo();
//		System.out.println(System.getProperty("file.encoding"));
		d.test6();
	}
	public void test6(){
		try {
			BufferedReader br =new BufferedReader(new FileReader("E:\\t.txt"));
			StringBuilder sb = new StringBuilder();
			String data;
			while((data=br.readLine())!=null){
				sb.append(data);
			}
			System.out.println(sb);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	public void test5(){
		
		try {
			InputStreamReader isr =new InputStreamReader(new FileInputStream("E:\\t.txt"));
			int data;
			while((data=isr.read())!=-1){
				System.out.print((char)data);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void test4(){
		try {
			FileOutputStream fo = new FileOutputStream("E:\\t.txt");
			String str = "好好学习，天天向上";
			fo.write(str.getBytes());
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void test3(){
		try {
			FileInputStream fis = new FileInputStream("E:\\t.txt");
			
//			System.out.println(fis.available());
			int data;
			byte[] by = new byte[fis.available()];
			while((data=fis.read(by))!=-1){
				System.out.print(new String(by));
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void test2(){
		File f=new File("C:\\");
		try {
//			f.createNewFile();
			String[] list = f.list();
			
			for (String string : list) {
				System.out.println(string);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	public void test1(){
		File file = new File("E:/ISO.EXE");
		System.out.println(file.getPath());
		System.out.println(file.getAbsolutePath());
		System.out.println(file.isFile());
		System.out.println(file.isDirectory());
		System.out.println(file.exists());
		System.out.println(file.getName());
		System.out.println(file.length());
		System.out.println(new Date(file.lastModified()));
//		System.out.println(file.delete());
		
	}
}
