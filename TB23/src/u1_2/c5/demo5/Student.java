package u1_2.c5.demo5;

public class Student {

	private String name;
	private int id;
	private int age;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	public boolean equals(Object obj){
		if (obj==this) {
			return true;
		}
		if (! (obj instanceof Student)) {
			return false;
		}
		Student stu=(Student)obj;
		if(stu.age==this.age && stu.id==this.id && stu.name.equals(this.name)){
			return true;
		}
		return false;
	}
	
}
