package u1_2.c5.demo6;

import u1_2.c5.demo4.Dog;
import u1_2.c5.demo4.Penguin;

public class Master {

	public void cure(Dog dog){
		dog.setHealth(100);
	}
	
	public void cure(Penguin pen){
		pen.setHealth(100);
	}
	
}
