package u1_2.c5.demo4;

public class Pet {

	private String name;
	private int health;
	private int love;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getLove() {
		return love;
	}

	public void setLove(int love) {
		this.love = love;
	}

	public void print() {
		System.out.println("宠物的自白：");
		System.out.println("我的名字叫" + name + ",我的健康值是" + health + ",我和主人的亲密度是" + love);
	}
	

}
