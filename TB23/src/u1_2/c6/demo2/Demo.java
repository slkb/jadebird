package u1_2.c6.demo2;

/**
 * 抽象类与抽象方法的使用 
 * 抽象类中可以没有抽象方法，但包含了抽象方法的类必须被定义为抽象类 
 * 如果子类没有实现父类的所有抽象方法，子类必须被定义为抽象类
 * 没有抽象构造方法，也没有抽象静态方法 
 * 抽象类中可以有非抽象的构造方法，创建子类的实例时可能调用
 * 
 * @author Administrator
 *
 */
public abstract class Demo {
	public Demo(){}
	public abstract void test();
}
class Child extends Demo{
	public Child(){
		super();
	}

	@Override
	public void test() {
	}
	
}
