package u1_2.c6.demo1;

/**
 * �γ�
 * 
 * @author Administrator
 *
 */
public class Car extends MotoVehicle {

	private String type;

	public Car(String brand, double perRent, String vehicleId, String type) {
		super(brand, perRent, vehicleId);
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public double calRent(int day) {
		double price = this.getPerRent() * day;
		if (day > 7 && day <= 30) {
			return price * 0.9;
		} else if (day > 30 && day <= 50) {
			return price * 0.8;
		} else if (day > 50) {
			return price * 0.7;
		}
		return price;
	}

}
