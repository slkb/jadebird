package u1_2.c6.demo1;

/**
 * 机动车
 * 
 * @author 孟凡鼎
 *
 */
public abstract class MotoVehicle {

	private String brand;// 品牌
	private double perRent;
	private String vehicleId;
	
	public MotoVehicle(){}
	
	public MotoVehicle(String brand,double perRent,String vehicleId){
		this.brand=brand;
		this.perRent=perRent;
		this.vehicleId=vehicleId;
	}
	
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public double getPerRent() {
		return perRent;
	}

	public void setPerRent(double perRent) {
		this.perRent = perRent;
	}

	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	/**
	 * 计算租金
	 * 
	 * @param day
	 * @return
	 */
	public abstract double calRent(int day);
}
